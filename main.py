import urllib
import operator
import re
import random
import json
from collections import Counter
# url must be given in the form 'http://...'
#url = 'http://allrecipes.com/Recipe/Slow-Cooker-Corned-Beef-and-Cabbage/Detail.aspx?soid=carousel_0_rotd&prop24=rotd'
#url = "http://allrecipes.com/Recipe/Biriyani/Detail.aspx?event8=1&prop24=SR_Title&e11=chicken%20biriyani&e8=Quick%20Search&event10=1&soid=sr_results_p1i1"
#url = "http://allrecipes.com/Recipe/Smoked-Salmon-Sushi-Roll/Detail.aspx?event8=1&prop24=SR_Title&e11=sushi&e8=Quick%20Search&event10=1&soid=sr_results_p1i9"
#url='http://allrecipes.com/Recipe/Chicken-Breasts-with-Balsamic-Vinegar-and-Garlic/Detail.aspx?soid=carousel_0_rotd&prop24=rotd'
#url='http://allrecipes.com/Recipe/Miso-Salmon/Detail.aspx?prop24=hn_slide1_Miso-Salmon&evt19=1'
#url='http://allrecipes.com/Recipe/Chicken-Breasts-with-Balsamic-Vinegar-and-Garlic/Detail.aspx?soid=carousel_0_rotd&prop24=rotd'
url='http://allrecipes.com/Recipe/Crispy-Orange-Beef/Detail.aspx?soid=carousel_0_rotd&prop24=rotd'
#transform='vegetarian'
transform='unhealthy'
data = urllib.urlopen(url)
from bs4 import BeautifulSoup
Soup=BeautifulSoup(data)

# taken from wikipedia list of utensils
utensils = [	"dish",
				"skillet",
				"baster",
				"bottle opener",
				"bowl",
				"bread knife",
				"tray",
				"knife",
				"cherry pitter",
				"colander",
				"pot",
				"saucepan",
				"corkscrew",
				"grater",
				"funnel",
				"ladel",
				"squeezer",
				"measuring jug",
				"measuring cup",
				"tenderiser",
				"thermometer",
				"mortar and pestle",
				"nutcracker",
				"peeler",
				"potato masher",
				"pizza cutter",
				"dutch oven",
				"rolling pin",
				"scissors",
				"spatula",
				"scoop",
				"tongs",
				"can opener",
				"spoon",
				"zester",
				"slow cooker"]
		
methods = [ "boil",
			"broil",
			"poach",
			"saute",
			"bake",
			"stew",
			"braise",
			"microwave",
			"steam",
			"poach",
			"stir fry",
			"deep fry",
			"dry fry",
			"shallow fry",
			"stir-fry",
			"deep-fry",
			"dry-fry",
			"shallow-fry",
			"grill",
			"roast",
			"slow cook",
			"slow-cook",
			"pan-seared",
			"pan seared"]

secondary_methods = [	"chop",
						"grate",
						"stir",
						"shake",
						"mine",
						"crush",
						"squeeze",
						"whisk",
						"peel"
						"cut"]
						
categories = ['meat',
			  'vegetable',
			  'green',
			  'mushroom',
			  'fruit',
			  'cereal',
			  'oil',
			  'spice',
			  'dairy',
			  'egg',
			  'protein',
			  'healthy',
			  'salty',
			  'sweet',
			  'spicy',
			  'acid',
			  'sauce',
			  'bean',
			  'nut',
			  'broth',
			  'vegetarian',
			  'italian',
			  'japanese',
			  'indian',
			  'healthy',
			  'glutenfree'
			  ]

meat = set(["chicken","bacon","beef","chorizo","dog","duck","frog",
		"game","goat","goose","ham","horse","kidney","liver","lamb","mutton",
		"pepperoni","pork","prosciutto","quail","rabbit","salami","sausage",
		"turkey","veal","venison","ground beef","beef heart","beef tongue",
		"tongue","heart","bison","buffalo","caribou","reindeer","marrow","blood",
		"moose","organ meats","organ","kangaroo","sweetbreads","tripe","turtle",
		"ostrich","pheasant","partridge","squab","cornish game hen","hen", "carp",
		"catfish","salmon","tilapia","grouper","bass","seabass","swordfish","shark","squid","octopus"
		"rock fish","sole","cod","haddock","halibut","anchovy","pike","flounder","lamprey",
		"herring","trout","rainbow trout","mahi-mahi","mahi","perch","pollock","mackerel",
		"tuna","walleye","whitefish","snapper","yellowtail","shrimp","eel","prawn","lobster",
		"crayfish","crab","blue crab","oyster","mussel","snail","abalone","clam","alligator",
		"crocodile","jellyfish","pulled pork","pulled chicken","pork chop","steak","fragois",
		"beef brisket","corned beef brisket","corned beef"])

#add unions - bean, legume, meat
protein = set(["paneer","tofu","soy","soy bean","edamame","tempeh","miso","natto","tamari","seitan","chicken","bacon","beef","chorizo","dog","duck","frog",
		"game","goat","goose","ham","horse","kidney","liver","lamb","mutton",
		"pepperoni","pork","prosciutto","quail","rabbit","salami","sausage",
		"turkey","veal","venison","ground beef","beef heart","beef tongue",
		"tongue","heart","bison","buffalo","caribou","reindeer","marrow","blood",
		"moose","organ meats","organ","kangaroo","sweetbreads","tripe","turtle",
		"ostrich","pheasant","partridge","squab","cornish game hen","hen", "carp",
		"catfish","salmon","tilapia","grouper","bass","seabass","swordfish","shark","squid","octopus"
		"rock fish","sole","cod","haddock","halibut","anchovy","pike","flounder","lamprey",
		"herring","trout","rainbow trout","mahi-mahi","mahi","perch","pollock","mackerel",
		"tuna","walleye","whitefish","snapper","yellowtail","shrimp","eel","prawn","lobster",
		"crayfish","crab","blue crab","oyster","mussel","snail","abalone","clam","alligator",
		"crocodile","jellyfish","pulled pork","pulled chicken","pork chop","steak","fragois",
		"beef brisket","corned beef brisket","corned beef"])

dairy = set(["milk","butter","cream","heavy cream","cheese","yogurt","butterfat","buttermilk",
			"condensed milk","cottage cheese","cream cheese","curd","custard","evaporated milk",
			"frozen custard","frozen yogurt","ice cream","paneer","chhena","khoa","lassi",
			"powedered milk","quark","shrikhand","strained yogurt","whipped cream","whey",
			"sour cream","doogh","daigo","chass","margarine","ghee"])

vegetable = set(["corn", "tomato", "green pepper", "red pepper", "mushroom", "kale", "spinach", "eggplant",
	"asparagus", "artichoke", "beet", "beetroot", "celery", "carrot", "collard green", "daikon",
	"cucumber", "onion", "garlic", "green onion", "spring onion", "leek", "okra", "pea", "broccoli",
	"potato", "pumpkin","olive", "radish", "lettuce", "rhubarb", "shallot", "brussel sprout", "sunchoke",
	"sweet potato", "tomatillo", "zucchini", "yam", "white asparagus", "bok choy", "arugula",
	"cabbage", "dill", "endive", "radicchio", "sorrel", "avocado", "aubergine", "brinjal", "squash",
	"bitter gourd", "butternut squash", "acorn squash", "sweet pepper", "banana pepper", "sweet corn",
	"caper", "cauliflower", "drumstick", "snap pea", "snow pea", "chive", "kohlrabi", "wild garlic", 
	"bamboo", "bamboo shoot", "cassava", "parsley", "turnip", "rutabaga", "taro", "nori", 
	"kombu", "hijiki", "ogonori", "wakame", "sea grape", "sea lettuce", "arame", "carola", "gim","seaweed",
	"mozuku", "aonori", "chard", "swiss chard", "spaghetti squash", "parsnip", "cactus","heart of palm"])

green = set(["kale", "spinach", "celery green", "beet green", "collard green", "leek green", "arugula", "bok choy",
	"cabbage", "endive", "dill", "radicchio", 'parsley', "chive", "wild garlic", "nori", 
	"kombu", "hijiki", "ogonori", "wakame", "sea grape", "sea lettuce", "arame", "carola", "gim",
	"mozuku", "aonori", "chard", "swiss chard","seaweed"])

oil = set(["coconut oil", "corn oil", "cottonseed oil", "olive oil", "palm oil", "peanut oil", 
	"grapeseed oil", "safflower oil", "sesame oil", "soybean oil", "sunflower oil", "mustard oil",
	"almond oil", "cashew oil", "hazelnut oil", "macadamia oil", 'pine nut oil', "walnut oil", "flaxseed oil",
	"argan oil", "cocoa butter", "hemp oil", "shea butter", "castor oil", "butter",
	"margarine", "earth balance", "ghee","salted butter", "unsalted butter"])

acid = set(["balsamic vinegar", "apple cider vinegar", "rice vinegar", "sherry", "red wine vinegar", "citric acid",
	"lime juice", "lemon juice", "grapefruit juice", "ascorbic acid", "orange juice", "lemon zest", "zest"
	"orange zest", "tamarind", "lemongrass", "preserved lemon", "wine","white vinegar"])

egg = set(["egg", "yolk", "white","egg white","egg yolk", "egg replacer", "tapioca powder"])

sauce = set(["soy sauce","marinara","cocktail sauce","barbeque sauce","duck sauce","salsa",
			"tomato sauce","sofrito","steak sauce","vinaigrette","worchestershire sauce",
			"gravy","poutine sauce","bordelaise sauce","aioli","mayonnaise","tartar sauce",
			"remoulade","buffalo sauce","chili sauce","tabasco sauce","pesto","pico de gallo",
			"caramel sauce","chocolate sauce","fudge sauce","applesauce","mushroom sauce",
			"yogurt sauce"])
			
cereal = set(["barley", "fonio", "maize", "pearl millet", "millet", "oats", "rolled oats", "quick oats",
	"palmer's grass", "rice", "white rice", "brown rice", "basmati rice", "jasmine rice", "rye", "sorghum",
	"spelt", "wild rice", "teff", "triticale", "wheat", "breadnut", "buckwheat", "cattail", "chia", "flax",
	"flaxseed", "amaranth", "kaniwa", "quinoa", "sesame", "arborio rice", "ariete rice", "baldo",
	"carnaroli", "lido rice", "maratelli rice", "originario rice", "padano rice", "ribe rice", "roma rice",
	"venere rice", "rissoto rice", "koshikari", "hitomebore", "hinohikari", "kinuhikari", "nihonbare",
	"sasanishiki", "hoshinoyume", "domannaka", "akitakomachi", "haenuki", "hanaechizen", "akebono rice",
	"asahi rice", "asahi", "akebono", "sticky rice", "red rice", "glutinous rice"])

pasta = set(["bigoli", "bucatini", "capelloini", "fedelini", "fusilli", "fusilli bucati", "matriciani",
	"pelizzoni", "perciatelli", "pici", "spaghetti", "spaghettini", "spaghettoni", "vermicelli", "vermicelloni",
	"ziti", "zitoni", "bavette", "bavettine", "ciriole", "fettuce", "fetuccine", "fettucelle", "lagane",
	"lasagne", "lasagna", "lasagnette", "lasagnotte", "linguettine", "linguine", "mafalde", "mafaldine",
	"pappardelle", "pillus", "pizzoccheri", "sagnarelli", "scialatelli", "scilatielli", "spaghetti all chitarra",
	"stringozzi","tagliatelle", "taglierini", "trenette", "tripoline", "calamarata", "calamaretti", "cannelloni", 
	"cavatappi", "cellentani", "chifferi", "ditalini", "elicoidali", "fagioloni", "fideua", "garganelli",
	"gemelli", "macaroni", "maccheronceeli", "maltagliati", "manicotti", "marziani", "mezzani", "mezze", "mezzi",
	"mostaccioli", "paccheri", "penne", "penne rigate", "penne lisce", "penne zita", "pennette", "pennoni", "rigatoncini",
	"rigatoni", "rotini", "spirali", "spiralini", "trenne", "trennette", "tortiglioni", "tuffoli", "campanelle", "capunti",
	"casarecce", "cavatelli", "cencioni", "conchiglie", "conchiglioni", "corzetti", "creste di galli", "croxetti",
	"farfalle", "farfalloni", "fiorentine", "fiori", "foglie d'ulivo", "gigli", "gramigna", "lanteme", "lumache",
	"lumaconi", "mandala", "marille", "orecchiette", "pipe", "quadrefiore", "radiatori", "ricciolini", "ricciutelle",
	"rotelle", "rotini", "soprese", "strozzapreti", "torchio", "trofie", "alfabeto", "anelli", "alphabet", "anelli", 
	"anellini", "conchigliette", "corallini", "ditali", "ditalini", "egg barley", "farfalline", "fideos", "fregula",
	"funghini", "grattini", "grattoni", "midolline", "occhi di pernice", "orzo", "pastina", "pearl", "puntine", "quadrettini",
	"risi", "semi di melone", "stelle", "stelline", "stortini", "tripolini", "agnolotti", "cannelloni", "cappelletti",
	"casoncelli", "fagottini", "maultasche", "mezzelune", "spaetzle", "pelmini", "pierogi", "ravioli",
	"sacchettini", "sacchettoni", "sacchetti", "tortellini", "tortelloni", "gnocchi", "passatelli", "spatzle"])

spice = set(["ajwain", "allspice", "anise", "aniseed", "asafoetida", "sweet basil", "lemon basil",
	"thai basil", "holy basil", "basil", "bay leaf", "black mustard", "brown mustard", "mustard", "caraway"
	"cardamom", "carob", "catnip", "cayenne", "cayenne pepper", "cassia", "celery leaf", "celery seed", "celery",
	"coriander leaf", "coriander seed", "coriander", "chervil", "chicory", "chili pepper", "chive",
	"cicely", "cilantro", "cinnamon", "white cinnamon", "clove", "cumin", "curry leaf", "dill", "dill seed",
	"elderflower", "epazote", "fennel", "fenugreek", "fingerroot", "galangal", "garlic", "elephant garlic",
	"ginger", "horseradish", "hyssop", "jasmine", "juniper", "jimbu", "kaffir lime", "kaffir lime leaves", "kaffir lime leaf",
	"kaffir lime lea", "kawakawa", "lavender", "lemon balm", "lemongrass", "lemon myrtle", "lemon verbena",
	'licorice', "lime flower", "lovage", "mace", "marjoram", "mastic", "mint", "white mustard", "nasturtium",
	"nutmeg", "neem", "oregano", "paprika", "parsley", "paracress", "black pepper", "white pepper", "green pepper",
	"peppermint", "rosemary", "rue", "safflower", "saffron", "sage", "sassafras", "savory", "sesame",
	"shiso", "sorrel", "spearmint", "star anise", "sumac", "spikenard", "tarragon", "thyme", "turmeric",
	"vanilla", "wasabi", "watercress", "wintergreen", "woodruff", "wormwood", "za'atar","salt","nanami","perilla",
	"chicken broth"])

italian = set(["salt","chicken","bacon","beef","game","ham","liver","kidney",
			   "pepperoni","pork","prosciutto","rabbit",'salami','sausage',
			   'veal','venison','ground beef','organ meats','organ','sweetbreads',
			   'tripe','pheasant','tilapia','squid','octopus','cuttlefish',
			   'anchovy','anchovies','clams','mussels','mullet','flounder'
			   ,'shrimp',
			   "milk","butter","cream","heavy cream","cheese",
			   "tomato", "green pepper", "red pepper", "mushroom",
			   "spinach", "eggplant", "asparagus", "artichoke",
			   "cucumber", "onion", "garlic", "green onion", "spring onion", "leek",
			   "broccoli","potato","zucchini","white asparagus","arugula",
			   "banana pepper","radish", "lettuce", "shallot", "brussel sprout",
			   "cabbage", "dill", "endive", "radicchio", "sorrel","caper","chive","wild garlic",
			   "ginger","parsley", "turnip", "rutabaga","parsnip","heart of palm","olive",
			   "spinach", "celery green", "beet green", "collard green", "leek green",
			   "dill","parsley",
			   "olive oil", "palm oil","grapeseed oil","mustard oil","almond oil",'pine nut oil',
			   "balsamic vinegar","sherry","red wine vinegar","wine","lime juice", "lemon juice"
			   ,"grapefruit juice","ascorbic acid","orange juice", "lemon zest", "zest","orange zest",
			   "preserved lemon",
			   "egg", "yolk", "white","egg white","egg yolk",
			   "anise", "aniseed","sweet basil", "lemon basil","holy basil", "basil"
			   ,"bay leaf","black mustard", "brown mustard", "mustard", "caraway",
			   "cayenne", "cayenne pepper","celery leaf", "celery seed", "celery",
			   "coriander leaf", "coriander seed", "coriander", "chervil", "chicory",
			   "chive","cicely", "cilantro", "cinnamon", "white cinnamon", "clove",
			   "dill", "dill seed","elderflower","fennel","garlic","hyssop",'licorice',
			   "mace","mastic", "mint", "white mustard","peppermint", "rosemary","sage",
			   "sesame","spearmint", "thyme","vanilla","watercress"])

indian = set([ "sweet potato","egg","lamb", "mutton", "chicken", "pomfret","prawn","hilsa","shrimp","curd","milk", 
	"condensed milk", "sour cream", "kulfi","yogurt","butterfat", "cottage cheese","lassi", "paneer",
	"ghee","clarified butter", "khoa","buttermilk","mustard oil", "coconut oil", "papad", "semolina", 
	"cardamom","raisins","saffron", "pistachios","almonds","lovage", "pomegranate","bay leaf","turmeric",
	"cinnamon", "coriander", "cloves", "cumin","curry leaves","fenugreek","garam masala","curry masala",
	"black pepper", "chilli", "saffron","fennel seed","tamarind","mustard","black mustard", "white mustard",
	"nigella", "ajwain", "bay leaf", "black mustard",  "caraway","cardamom", "coriander leaf", "coriander seed", 
	"chili pepper", "cilantro", "white cinnamon", "clove", "cumin", "curry leaf", "dill", "dill seed",
	"epazote", "fennel", "fennel seed","ginger", "jimbu", "kaffir lime", "kaffir lime leaves", "kaffir lime leaf",
	"kaffir lime lea",  "lime flower","mint", "white mustard","nutmeg", "neem", "black pepper", "white pepper",
	"rue", "safflower", "saffron", "turmeric","asafetida","potato","coconut","gourd","pumpkin", "tomato", 
	"green pepper", "red pepper", "mushroom","spinach", "eggplant","asparagus", "beet", "beetroot","carrot",
	"cucumber", "onion", "garlic","okra", "pea","radish","cabbage", "brinjal", "squash",
	"bitter gourd", "butternut squash","acorn squash",  "cauliflower", "drumstick", "kohlrabi", "wild garlic", 
	"ginger", "parsley", "turnip", "lime juice", "lemon juice","dal","masoor dal","mung dal","chana",
	"toor dal","urad dal"])

salt = set(["salt and pepper","salt","pepper"])
veggie = set(["eggplant","mushroom","hummus"])	
japanese = set(["salmon","rice","soba","somen","ramen","udon","yakisoba","cucumber","eggplant","nanami","perilla"
				"shishito","kabocha","shiro-uri","komatsuna","mizuna","napa cabbage","ginger",
				"takana","sweet potato","taro","sesame","seaweed","edamame","soy sauce",
				"miso","sake",
				"beef","chicken","pork","tuna","mackerel","herring","scallop","oyster","seaweed"])
	
healthy = set(["chicken","turkey","ostrich","carp",
		"catfish","salmon","tilapia","grouper","bass","seabass","swordfish","shark","squid","octopus"
		"rock fish","sole","cod","haddock","halibut","anchovy","pike","flounder","lamprey",
		"herring","trout","rainbow trout","mahi-mahi","mahi","perch","pollock","mackerel",
		"tuna","walleye","whitefish","snapper","yellowtail","shrimp","eel","prawn","lobster",
		"crayfish","crab","blue crab","oyster","mussel","snail","abalone","clam","yogurt","cottage cheese",
		"milk","strained yogurt","corn", "tomato", "green pepper", "red pepper", "mushroom", "kale", "spinach", "eggplant",
	"asparagus", "artichoke", "beet", "beetroot", "celery", "carrot", "collard green", "daikon",
	"cucumber", "onion", "garlic", "green onion", "spring onion", "leek", "okra", "pea", "broccoli",
	"potato", "pumpkin","olive", "radish", "lettuce", "rhubarb", "shallot", "brussel sprout", "sunchoke",
	"sweet potato", "tomatillo", "zucchini", "yam", "white asparagus", "bok choy", "arugula",
	"cabbage", "dill", "endive", "radicchio", "sorrel", "avocado", "aubergine", "brinjal", "squash",
	"bitter gourd", "butternut squash", "acorn squash", "sweet pepper", "banana pepper", "sweet corn",
	"caper", "cauliflower", "drumstick", "snap pea", "snow pea", "chive", "kohlrabi", "wild garlic", 
	"bamboo", "bamboo shoot", "cassava", "ginger", "parsley", "turnip", "rutabaga", "taro", "nori", 
	"kombu", "hijiki", "ogonori", "wakame", "sea grape", "sea lettuce", "arame", "carola", "gim","seaweed",
	"mozuku", "aonori", "chard", "spaghetti squash", "parsnip", "cactus","heart of palm","tomato sauce",
	"vinaigrette","pesto","pico de gallo","applesauce","mushroom sauce","yogurt sauce",
	"ajwain", "celery green","barley", "fonio", "maize", "pearl millet", "millet", "oats", "rolled oats", "quick oats",
	"palmer's grass"])
	
unhealthy = set(['frozen yogurt', 'egg replacer', 'ham', "ghee", 'steak sauce', 'organ meats', 'yakisoba', 'dal', 'corn oil',
	'game',  'duck sauce', 'ramen', 'organ', 'crocodile', 'prosciutto',  'bison', 'heavy cream', 'gravy', 'sour cream', 'turtle', 
	'pork', 'clams', 'bacon',  'kulfi', 'yolk', 'goose',  'khoa',  'ice cream', 'walnut oil', 'sausage', 'mutton',  'pork chop', 
	'condensed milk','octopus',  'mussels', 'kangaroo', 'salted butter', 'mullet',  'steak',  'egg yolk',  'pepperoni', 'chorizo', 
	'tongue', 'cheese', 'clarified butter', 'shrikhand',  'scallop', 'tamari','ground beef', 'duck', 'soy',  'custard', 'horse', 
	'mizuna', 'beef', 'alligator',  'frog', 'buttermilk', 'reindeer',  'papad',  'tripe', 'shea butter', 'kabocha', 'cocoa butter',
	'cream cheese', 'paneer',  'pulled pork', 'liver', 'rabbit',    'soba', 'butterfat',  'salami', 'buffalo', 'heart', 'chocolate sauce',
	'shiro-uri',  'quail', 'beef tongue', 'kidney', 'lamb', 'seitan',  'sweetbreads', 'venison', 'fragois', 'squab', 'fudge sauce', 
	'mayonnaise',  'frozen custard', 'goat', 'whipped cream', 'unsalted butter', 'moose', 'buffalo sauce', 'blood', 'marrow', 'barbeque sauce',
	'cuttlefish', 'jellyfish', 'butter', 'beef heart', 'caribou', 'dog',  'udon', 'somen', 'veal', 'caramel sauce', 'komatsuna',"corned beef brisket",
	"beef brisket","brisket"])

cuisines = ["japanese","italian","indian","no_cuisine"]

neutral = (meat|vegetable|green|oil|acid|egg|spice|protein|dairy|japanese|indian|italian|salt|cereal|pasta)-(healthy|unhealthy)
no_cuisine =(meat|vegetable|green|oil|acid|egg|spice|protein|dairy|japanese|indian|italian|salt|cereal|pasta)-(japanese|italian|indian)

categorySets=[meat,vegetable,green,oil,acid,egg,spice,protein,dairy,sauce,japanese,indian,italian,salt,healthy,unhealthy,cereal,pasta,neutral,no_cuisine]
categoryNames=["meat","vegetable","green","oil","acid","egg","spice","protein","dairy","sauce","japanese","indian","italian","salt","healthy","unhealthy","cereal","pasta","neutral","no_cuisine"]

class Ingredient:
	def __init__(self,name,quantity,measurement,descriptor):
		self.name=name
		self.quantity=quantity
		self.measurement=measurement
		self.descriptor=descriptor
	def disp(self):
		print "name: ",self.name
		print "quantity: ",self.quantity
		print "measurement: ", self.measurement
		print "descriptor: ", self.descriptor
		print

# dynamically create dictionary of all ingredients listed above
# key is a string containing the ingredient name
# value is a list of categories (strings) that the ingredient belongs to
ingred_dict={}
i=0
for category in categorySets:
	categoryName=categoryNames[i]
	for entry in category:
		if entry in ingred_dict:
			# if this entry is already in the dictionary
			oldlist=ingred_dict[entry]
			oldlist.append(categoryName)
			newlist=oldlist
			ingred_dict[entry]=newlist
		else:
			ingred_dict[entry]=[categoryName]
	i=i+1

# parse html to get ingredients for this recipe
ingreds=[]
for ingred in Soup.find_all("p", {"itemprop": "ingredients"}):
	try:
		amount=ingred.find("span", {"id": "lblIngAmount"}).contents[0].split(' ')
		if len(amount)==1:
			quantity=amount[0]
			measurement='discrete'
		else:
			quantity=amount[0]
			del amount[0]
			measurement=' '.join(amount)
	except:
		quantity='none'
		measurement='none'
	string=ingred.find("span", {"id": "lblIngName"}).contents[0]
	found=False
	for i in [4,3,2,1]:
		for key in ingred_dict:
			if len(key.split(' '))>=i:
				if key in string:
					name=key
					string=string.replace(key,"",1)
					descriptor=string
					found=True
					break
	if not found:
		name=string
		descriptor='none'
	new_ingred=Ingredient(name,quantity,measurement,descriptor)
	ingreds.append(new_ingred)

directions=[]
secondary_method_list=[]
uten_list=[]
method_dict={}

# see if any cooking method is contained in recipe name
# if so weight that method high in consideration for primary method
recipeName = url.split('/')[4].split('-')
recipeName = ' '.join(recipeName).lower()
for method in methods:
	if method in recipeName:
		method_dict[method] = 10

# parse the directions, also try to determine primary method		
for direction in Soup.find_all("span", {"class": "plaincharacterwrap break"}):
	string=str(direction.contents[0])
	for item in utensils:
		if item in string:
			if item not in uten_list:
				uten_list.append(item)
	for method in methods:
		if method in string:
			if method in method_dict:
				method_dict[method] = method_dict[method] + 1
			else:
				method_dict[method] = 1
	for method in secondary_methods:
		if method in string:
			if method not in secondary_method_list:
				secondary_method_list.append(method)
	directions.append(string)

# assign primary method
if len(method_dict)>0:
	primary_method=max(method_dict.iteritems(), key=operator.itemgetter(1))[0]
else:
	primary_method="no primary method"

# calculate the most likely "cuisine" of this dish
lst = {}
for cuisine in cuisines:
	lst[cuisine] = 0
for i,ingred in enumerate(ingreds):
	if ingred.name in ingred_dict:
		categories=ingred_dict[ingred.name]
		for cuisine in cuisines:
			if cuisine in categories and "meat" in categories:
				lst[cuisine] += 5
			elif cuisine in categories:
				lst[cuisine] += 1

count = Counter(lst)
most_likely_cuis = list(count.most_common(1))[0][0]
print "Recipe identified as cuisine: ",most_likely_cuis

def healthy_unhealthy_transform(ingredient_list,old_class,new_class,most_likely_cuis):
	# replace any ingredients in ingredient list that belong to
	# old class with an ingredient belonging to new class
	# that is preferably a member of most_likely_cuis class
	# if cuisine_transform is True, consider cuisine sets in the intersection
	old_idx = categoryNames.index(old_class)
	old_set = categorySets[old_idx]
	new_idx = categoryNames.index(new_class)
	new_set = categorySets[new_idx]
	subs = [i.name for i in ingreds]
	new_list=[]
	print "Performing transformation: ",transform
	for i,ingred in enumerate(ingredient_list):
		if ingred.name in ingred_dict:
			if ingred.name in old_set:
				categories=ingred_dict[ingred.name]
				#intersect ingred for all categories
				catSets = []
				for c in categories:
					if c != old_class and c not in cuisines:
						idx = categoryNames.index(c)
						catSets.append(categorySets[idx])
				catSets.append(new_set)
				catSets.append(categorySets[categoryNames.index(most_likely_cuis)])
				catIntersection = set.intersection(*catSets)
				if list(catIntersection)==[]:
					# if nothing in the intersection
					# try removing the calculated cuisine type
					# from the intersection
					del catSets[-1]
					catSets.append(neutral)
					catIntersection = set.intersection(*catSets)
					if list(catIntersection)==[]:
						print "no suitable transformation"
				print "Old Ingredient: ",ingred.name
				subChoices = list(catIntersection)
				random.shuffle(subChoices)
				# only replace ingredient if it is not in the list of possible replacements
				if subChoices!=[]:
					if ingred.name not in subChoices:
						alternative = subChoices[0]						
					else:
						print "No suitable replacement"
						alternative=ingred.name
				else:
					print "No suitable replacement"
					alternative=ingred.name
				print "New Ingredient: ",alternative
				print
				new_ingred=Ingredient(alternative,ingred.quantity,ingred.measurement,'none')
				new_list.append(new_ingred)
				for i,direction in enumerate(directions):
					if ingred.name in direction:
						directions[i]=direction.replace(ingred.name,alternative)
			else:
				new_list.append(ingred)
		else:
			new_list.append(ingred)
	return new_list
	
def cuisine_transform(ingredient_list,new_cuisine):
	# replace any ingredients in ingredient list that belong to
	# old class with an ingredient belonging to new class
	# that is preferably a member of most_likely_cuis class
	new_idx = categoryNames.index(new_cuisine)
	new_set = categorySets[new_idx]
	subs = [i.name for i in ingreds]
	new_list=[]
	print "Performing transformation: ",transform
	for i,ingred in enumerate(ingredient_list):
		if ingred.name in ingred_dict:
			if ingred.name not in no_cuisine:
				categories=ingred_dict[ingred.name]
				# intersect ingred for all categories
				# except for cuisine catecories
				catSets = []
				for c in categories:
					if c not in ["italian","japanese","indian"]:
						idx = categoryNames.index(c)
						catSets.append(categorySets[idx])
				print new_cuisine
				catSets.append(new_set)
				catIntersection = set.intersection(*catSets)
				if list(catIntersection)==[]:
					# if nothing in the intersection
					# try to replace with something from a
					# different health class
					# because we don't really care about that
					for health_set in [healthy,unhealthy,neutral]:
						if health_set in catSets:
							catSets.remove(health_set)
					catIntersection = set.intersection(*catSets)
					# if it's still empty then there is no suitable 
					# transform for this ingredient
					if list(catIntersection)==[]:
						print "No Suitable Substitution"
				print "Old Ingredient: ",ingred.name
				subChoices = list(catIntersection)
				random.shuffle(subChoices)
				# only replace ingredient if it is not in the 
				# list of possible replacements
				if subChoices!=[]:
					if ingred.name not in subChoices:
						alternative = subChoices[0]
					else:
						print "No suitable replacement"
						alternative=ingred.name
				else:
					print "No suitable replacement"
					alternative=ingred.name
				print "New Ingredient: ",alternative
				print
				new_ingred=Ingredient(alternative,ingred.quantity,ingred.measurement,'none')
				for i,direction in enumerate(directions):
					if ingred.name in direction:
						directions[i]=direction.replace(ingred.name,alternative)
				new_list.append(new_ingred)
			else:
				new_list.append(ingred)
		else:
			new_list.append(ingred)
	return new_list
	
def vegetarian_transform(ingredient_list,most_likely_cuis):
	# if any ingredient has meat, replace it with the ingredient
	# most similar to it (intersection of categories it belongs to
	# not including meat), also belonging to the dishes "cuisine"
	new_list=[]
	print "Performing transformation: ",transform
	for ingred in ingredient_list:
		if ingred.name in ingred_dict:
			alt = []
			categories=ingred_dict[ingred.name]
			if "meat" in categories:
				for cuisine in cuisines:
					if cuisine in categories:
						alt += [cuisine]
				print "Old Ingedient: ",ingred.name
				alt = list(set(alt))
				idx = categoryNames.index(most_likely_cuis)
				vegetarian = list((protein - meat) & categorySets[idx])
				print vegetarian
				if vegetarian == []:
					alternative = random.choice(list(veggie & categorySets[idx]))
				else:
					alternative = random.choice(vegetarian)
				print "New Ingredient: ",alternative,"\n"
				new_ingred=Ingredient(alternative,ingred.quantity,ingred.measurement,'none')
				for i,direction in enumerate(directions):
					if ingred.name in direction:
						directions[i]=direction.replace(ingred.name,alternative)
				new_list.append(new_ingred)
			else:
				new_list.append(ingred)
		else:
			new_list.append(ingred)
	return new_list

def parse_recipe(urlstring):
	data = urllib.urlopen(urlstring)
	Soup=BeautifulSoup(data)	
	ingreds=[]
	for ingred in Soup.find_all("p", {"itemprop": "ingredients"}):
		try:
			amount=ingred.find("span", {"id": "lblIngAmount"}).contents[0].split(' ')
			if len(amount)==1:
				quantity=amount[0]
				measurement='discrete'
			else:
				quantity=amount[0]
				del amount[0]
				measurement=' '.join(amount)
		except:
			quantity='none'
			measurement='none'
		string=ingred.find("span", {"id": "lblIngName"}).contents[0]
		found=False
		for i in [4,3,2,1]:
			for key in ingred_dict:
				if len(key.split(' '))>=i:
					if key in string:
						name=key
						string=string.replace(key,"",1)
						descriptor=string
						found=True
						break
		if not found:
			name=string
			descriptor='none'
		new_ingred=Ingredient(name,quantity,measurement,descriptor)
		ingreds.append(new_ingred)

	directions=[]
	secondary_method_list=[]
	uten_list=[]
	method_dict={}

	# see if any cooking method is contained in recipe name
	# if so weight that method high in consideration for primary method
	recipeName = url.split('/')[4].split('-')
	recipeName = ' '.join(recipeName).lower()
	for method in methods:
		if method in recipeName:
			method_dict[method] = 10

	# parse the directions, also try to determine primary method		
	for direction in Soup.find_all("span", {"class": "plaincharacterwrap break"}):
		string=str(direction.contents[0])
		for item in utensils:
			if item in string:
				if item not in uten_list:
					uten_list.append(item)
		for method in methods:
			if method in string:
				if method in method_dict:
					method_dict[method] = method_dict[method] + 1
				else:
					method_dict[method] = 1
		for method in secondary_methods:
			if method in string:
				if method not in secondary_method_list:
					secondary_method_list.append(method)
		directions.append(string)

	# assign primary method
	if len(method_dict)>0:
		primary_method=max(method_dict.iteritems(), key=operator.itemgetter(1))[0]
	else:
		primary_method="no primary method"
	json_str="{ 'ingredients': ["
	ret_dict = {}
	ret_dict['ingredients'] = []
	for ingred in ingreds:
		ingredDict = {}
		ingredDict['name'] = ingred.name
		ingredDict['quantity'] = ingred.quantity
		ingredDict['measurement'] = ingred.measurement
		ingredDict['descriptor'] = ingred.descriptor
		ret_dict['ingredients'].append(ingredDict)
		json_str=json_str+"{ 'name':'"+ingred.name+"','quantity':'"+ingred.quantity+"','measurement':'"+ingred.measurement+"','descriptor':'"+ingred.descriptor+"'},"
	# remove last comma
	json_str=json_str[:-1]
	json_str=json_str+"],'cooking method':'"+primary_method+"',"
	ret_dict['cooking method'] = primary_method
	ret_dict['cooking tools'] = []
	json_str=json_str+"'cooking tools':["
	for tool in uten_list:
		ret_dict['cooking tools'].append(tool)
		json_str=json_str+"'"+tool+"',"
	json_str=json_str[:-1]
	json_str=json_str+"],}"
	return ret_dict

def print_recipe():
	print "Primary Cooking Method: ",primary_method
	print "Secondary Methods: ",secondary_method_list
	for i,ingred in enumerate(ingreds):
		print "Ingredient ",i
		ingred.disp()
	print "Directions\n---"
	for i,direction in enumerate(directions):
		print "Step ",i+1
		print direction
		print
	print "Utensils:\n---\n",uten_list

print "Old Recipe: ",recipeName,"\n======================"
print_recipe()
print "======================\n"

# build the json output of new recipe
json_str="{ 'ingredients': ["
for ingred in ingreds:
	json_str=json_str+"{ 'name':'"+ingred.name+"','quantity':'"+ingred.quantity+"','measurement':'"+ingred.measurement+"','descriptor':'"+ingred.descriptor+"'},"
# remove last comma
json_str=json_str[:-1]
json_str=json_str+"],'cooking method':'"+primary_method+"',"
json_str=json_str+"'cooking tools':["
for tool in uten_list:
	json_str=json_str+"'"+tool+"',"
json_str=json_str[:-1]
json_str=json_str+"],}"

print "Original Recipe: JSON output:\n---"
print json.dumps(json_str)
old_json=json_str
	
# perform desired transform
if transform=='vegetarian':
	ingreds=vegetarian_transform(ingreds,most_likely_cuis)
elif transform=="healthy":
	ingreds=healthy_unhealthy_transform(ingreds,"unhealthy","healthy",most_likely_cuis)
elif transform=="unhealthy":
	ingreds=healthy_unhealthy_transform(ingreds,"healthy","unhealthy",most_likely_cuis)	
else:
	ingreds=cuisine_transform(ingreds,transform)
	
print "Transformed Recipe: ",transform,"\n ====================="
print_recipe()
print "======================\n"

# build the json output of new recipe
json_str="{ 'ingredients': ["
for ingred in ingreds:
	json_str=json_str+"{ 'name':'"+ingred.name+"','quantity':'"+ingred.quantity+"','measurement':'"+ingred.measurement+"','descriptor':'"+ingred.descriptor+"'},"
# remove last comma
json_str=json_str[:-1]
json_str=json_str+"],'cooking method':'"+primary_method+"',"
json_str=json_str+"'cooking tools':["
for tool in uten_list:
	json_str=json_str+"'"+tool+"',"
json_str=json_str[:-1]
json_str=json_str+"],}"

print "Transformed Recipe: JSON output:\n---"
print json.dumps(json_str)
new_json=json_str
